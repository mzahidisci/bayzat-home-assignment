import { useState } from 'react'
import './App.scss'
import Card from './Card'
import { Movie, movies } from './movies'

function App() {
    const [leftList, setLeftList] = useState<Movie[]>(movies.slice(0, 12))
    const [rightList, setRightList] = useState<Movie[]>([])
    const [search, setSearch] = useState<string>('')
    const [currPage, setCurrPage] = useState(1)

    const onAddClick = (movie: Movie) => {
        rightList.push(movie)
        setRightList(rightList)

        const newLeftList = leftList.filter((item) => item.id !== movie.id)
        setLeftList(newLeftList)
    }

    const onRemoveClick = (movie: Movie) => {
        leftList.push(movie)
        setLeftList(leftList)

        const newRightList = rightList.filter((item) => item.id !== movie.id)
        setRightList(newRightList)
    }

    const onScroll = () => {
      let newArray = movies.slice(currPage * 12, (currPage + 1) * 12)
      setLeftList([...leftList, ...newArray])
      setCurrPage(currPage + 1);
    }

    return (
        <div className="App">
            <div>
                <h3>Saved Movies</h3>
                <div id="right-movie-container">
                    {}
                    {rightList.length > 0 ? (
                        rightList.map((movie, index) => {
                            return (
                                <Card
                                    key={index.toString()}
                                    isLeft={false}
                                    movie={movie}
                                    onRemoveClick={onRemoveClick}
                                    onAddClick={onAddClick}
                                    search={search}
                                ></Card>
                            )
                        })
                    ) : (
                        <div
                            style={{
                                textAlign: 'center',
                                width: '100%',
                                paddingBottom: '1rem',
                            }}
                        >
                            No Movies
                        </div>
                    )}
                </div>
            </div>
            <div>
                <input
                    placeholder="Type for searching..."
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    style={{padding:'0.5rem 1rem',margin:'1rem',borderRadius:'0.5rem',border:'0.5px solid grey'}}
                />
            </div>
            <div
                id="left-movie-container"

            >
                {leftList.map((movie, index) => {
                    return (
                        <Card
                            key={index.toString()}
                            isLeft={true}
                            movie={movie}
                            onRemoveClick={onRemoveClick}
                            onAddClick={onAddClick}
                            search={search}
                        ></Card>
                    )
                })}
            </div>{
               movies.slice(currPage * 12, (currPage + 1) * 12).length > 0 ? <button style={{padding:'0.5rem 1rem',marginBottom:'2rem '}} onClick={onScroll}>Load more</button> : <>END!!!</>
            }
            

        </div>
    )
}

export default App
