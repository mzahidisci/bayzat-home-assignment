import { format, fromUnixTime } from 'date-fns'
import { Movie } from './movies'
interface ICardProps {
    onRemoveClick: (movie: Movie) => void
    onAddClick: (movie: Movie) => void
    movie: Movie
    isLeft: Boolean
    search: string
}
const Card: React.FC<ICardProps> = ({
    onRemoveClick,
    onAddClick,
    movie,
    isLeft,
    search,
}) => {
    if (!movie.title.toLowerCase().includes(search.toLowerCase()) && isLeft) {
        return <></>
    }
    return (
        <div
            id="movie"
       
            // onMouseOver={(e) => {
            //     const el = e.currentTarget
            //     let l = 0
            //     function updateColor(newl: number) {
            //         l = newl
            //         el.style.backgroundColor = `#c86464${20 + l * 5}`
            //         if (l < 10) {
            //             setTimeout(() => updateColor(l + 1), 25)
            //         }
            //     }
            //     setTimeout(() => updateColor(l + 1), 25)
            // }}
            // onMouseLeave={(e) =>
            //     (e.currentTarget.style.backgroundColor = '#c8646420')
            // }
        >
                   {isLeft ? (
                    <button id="add-button" onClick={() => onAddClick(movie)}>
                        Add
                    </button>
                ) : (
                    <button
                        id="remove-button"
                        onClick={() => onRemoveClick(movie)}
                    >
                        Remove
                    </button>
                )}
            <div style={{height:'100%'}}>
                <img src={movie.poster} style={{ height: '100%' }} alt="" />
            </div>
            <div
                id="movie-info"
            >
                <div id="movie-title" >
                    {movie.title}
                </div>
                <div title={movie.overview} id="movie-overview">
                    {movie.overview}
                </div>
                <div>
                    Release date:{' '}
                    {format(fromUnixTime(movie.release_date),'yyyy-MM-dd')
                    }{' '}
                </div>
       
            </div>
        </div>
    )
}

export default Card
